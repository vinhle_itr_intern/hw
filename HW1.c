#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include<conio.h>

// There are two versions of HW1:
// VERSION 1 (VER1) and VERSION 2 (VER21 && VER22)

#define VER1
//#define VER22  
//#define VER21
void Reverse_String(char *str1);
int main()
{
    // Define Variables
    char str[100],*pstr, storage[100];
    // Input String
    printf("Input String: ");
    scanf("%s",&str);
  
#ifdef VER1
    pstr=str;
    for( uint8_t i = 0; i < 100; i++)
    {
        storage[i]=str[i];
    }
    uint16_t len = strlen(storage);
    for (uint8_t k = 0;k<len;k++)
    {
        pstr[k]=storage[len-1-k];
    }
    printf("Output: %s",str);
#endif
#ifdef VER21
    Reverse_String(str);
    printf("Output: %s",str);
#endif  
}
#ifdef VER22
void Reverse_String(char *str1)
{
    char storage [100];
    for( uint8_t i = 0; i < 100; i++)
    {
        storage[i]=str1[i];
    }
    uint16_t len = strlen(storage);
    for (uint8_t k = 0;k<len;k++)
    {
        str1[k]=storage[len-1-k];
    }
}
#endif


