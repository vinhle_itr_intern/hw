#include "unity.h"   // Unit test library, provides helper 
#include "cbuffer.h"
// Include the tested component here

/* Test Setup ----------------------------------------------------------- */
void setUp(void)
{
}

void tearDown(void)
{
}
void test_write_data(void)
{
    cbuffer_t cb;
    uint8_t cb_buff[8];
    cb_init(&cb, cb_buff, 8);
    uint8_t a[5] = { 1, 1, 3, 3, 4};
    cb_write(&cb, a, 5);
    TEST_ASSERT_EQUAL_UINT32(5,cb.writer);
    uint8_t e[5] = { 1, 2, 3, 4, 5};
    cb_write(&cb,e,5);
    TEST_ASSERT_EQUAL_UINT32(2, cb.writer);
    
}
void test_read_data(void)
{
    cbuffer_t cb;
    uint8_t cb_buff[8];
    cb_init(&cb, cb_buff, 8);
    uint8_t b[5] = {0,0,0,0,0};
    uint8_t cb_read_index = cb_read(&cb, b, 3);
    TEST_ASSERT_EQUAL_UINT32(3, cb_read_index);
    TEST_ASSERT_LESS_OR_EQUAL_UINT(5,cb_read_index);
}
void test_count_data(void)
{
    cbuffer_t cb;
    uint8_t cb_buff[10];
    cb_init(&cb, cb_buff, 10);
    uint8_t a[5] = { 1, 1, 3, 3, 4};
    cb_data_count(&cb);
    uint8_t e[5] = { 1, 2, 3, 4, 5};
    cb_write(&cb,e,5);
    uint8_t b[3] = {0,0,0};
    cb_read(&cb, b, 3);
    uint8_t cb_count_index = cb_data_count(&cb);
    TEST_ASSERT_EQUAL_UINT32(7, cb_count_index);    

}

void test_space_data(void)
{
    cbuffer_t cb;
    uint8_t cb_buff[8];
    cb_init(&cb, cb_buff, 8);
    uint8_t a[5] = { 1, 1, 3, 3, 4};
    cb_write(&cb, a, 5);
    uint8_t e[5] = { 1, 2, 3, 4, 5};
    cb_write(&cb,e,5);
     uint8_t b[5] = {0,0,0,0,0};
    uint8_t cb_read_index = cb_read(&cb, b, 3);
    uint8_t cb_space_index = cb_space_count(&cb);
    TEST_ASSERT_EQUAL_UINT8(3, cb_space_index);
}

void test_clear_buffer(void)
{
    cbuffer_t cb;
    uint8_t cb_buff[8];
    cb_init(&cb, cb_buff, 8);
    uint8_t a[5] = { 1, 1, 3, 3, 4};
    cb_write(&cb, a, 5);
    cb_clear(&cb);
    TEST_ASSERT_EQUAL_UINT8(0, cb.writer);
}