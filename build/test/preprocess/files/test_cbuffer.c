#include "src/cbuffer.h"
#include "C:/Ruby30-x64/lib/ruby/gems/3.0.0/gems/ceedling-0.31.1/vendor/unity/src/unity.h"






void setUp(void)

{

}



void tearDown(void)

{

}

void test_write_data(void)

{

    cbuffer_t cb;

    uint8_t cb_buff[8];

    cb_init(&cb, cb_buff, 8);

    uint8_t a[5] = { 1, 1, 3, 3, 4};

    cb_write(&cb, a, 5);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT32)((5)), (UNITY_INT)(UNITY_UINT32)((cb.writer)), (

   ((void *)0)

   ), (UNITY_UINT)(20), UNITY_DISPLAY_STYLE_UINT32);

    uint8_t e[5] = { 1, 2, 3, 4, 5};

    cb_write(&cb,e,5);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT32)((2)), (UNITY_INT)(UNITY_UINT32)((cb.writer)), (

   ((void *)0)

   ), (UNITY_UINT)(23), UNITY_DISPLAY_STYLE_UINT32);



}

void test_read_data(void)

{

    cbuffer_t cb;

    uint8_t cb_buff[8];

    cb_init(&cb, cb_buff, 8);

    uint8_t b[5] = {0,0,0,0,0};

    uint8_t cb_read_index = cb_read(&cb, b, 3);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT32)((3)), (UNITY_INT)(UNITY_UINT32)((cb_read_index)), (

   ((void *)0)

   ), (UNITY_UINT)(33), UNITY_DISPLAY_STYLE_UINT32);

    UnityAssertGreaterOrLessOrEqualNumber((UNITY_INT) ((5)), (UNITY_INT) ((cb_read_index)), UNITY_SMALLER_OR_EQUAL, (

   ((void *)0)

   ), (UNITY_UINT)(34), UNITY_DISPLAY_STYLE_UINT);

}

void test_count_data(void)

{

    cbuffer_t cb;

    uint8_t cb_buff[10];

    cb_init(&cb, cb_buff, 10);

    uint8_t a[5] = { 1, 1, 3, 3, 4};

    cb_data_count(&cb);

    uint8_t e[5] = { 1, 2, 3, 4, 5};

    cb_write(&cb,e,5);

    uint8_t b[3] = {0,0,0};

    cb_read(&cb, b, 3);

    uint8_t cb_count_index = cb_data_count(&cb);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT32)((7)), (UNITY_INT)(UNITY_UINT32)((cb_count_index)), (

   ((void *)0)

   ), (UNITY_UINT)(48), UNITY_DISPLAY_STYLE_UINT32);



}



void test_space_data(void)

{

    cbuffer_t cb;

    uint8_t cb_buff[8];

    cb_init(&cb, cb_buff, 8);

    uint8_t a[5] = { 1, 1, 3, 3, 4};

    cb_write(&cb, a, 5);

    uint8_t e[5] = { 1, 2, 3, 4, 5};

    cb_write(&cb,e,5);

     uint8_t b[5] = {0,0,0,0,0};

    uint8_t cb_read_index = cb_read(&cb, b, 3);

    uint8_t cb_space_index = cb_space_count(&cb);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((3)), (UNITY_INT)(UNITY_UINT8 )((cb_space_index)), (

   ((void *)0)

   ), (UNITY_UINT)(64), UNITY_DISPLAY_STYLE_UINT8);

}



void test_clear_buffer(void)

{

    cbuffer_t cb;

    uint8_t cb_buff[8];

    cb_init(&cb, cb_buff, 8);

    uint8_t a[5] = { 1, 1, 3, 3, 4};

    cb_write(&cb, a, 5);

    cb_clear(&cb);

    UnityAssertEqualNumber((UNITY_INT)(UNITY_UINT8 )((0)), (UNITY_INT)(UNITY_UINT8 )((cb.writer)), (

   ((void *)0)

   ), (UNITY_UINT)(75), UNITY_DISPLAY_STYLE_UINT8);

}
